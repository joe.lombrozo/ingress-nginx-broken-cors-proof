test_file := "test.bin"
helm_name := "ingress-nginx"

# spin up the cluster
init:
    minikube start --kubernetes-version 1.21.14

_curl *args:
    #!/usr/bin/env bash
    url=$(minikube service ingress-nginx-controller --url | head -n1)
    curl -v ${url} --header Host:localhost --header Origin:http://localhost1 {{ args }}

_data:
    #!/usr/bin/env bash
    if [ ! -f {{ test_file }} ]; then
        echo "generating {{ test_file }} ..."
        dd if=/dev/urandom of={{ test_file }} bs=1M count=5
    fi

# make a GET request to see CORS headers when the pod is hit successfully
test-get: _curl

# make a POST request to see CORS headers when nginx rejects the request
test-post: _data (_curl "--data" "@" + test_file)

_install-chart NAME PATH:
    helm dep up {{ PATH }}
    helm delete {{ NAME }} || true
    helm upgrade --install {{ NAME }} {{ PATH }}

# install the old 3.41.0 ingress-nginx chart
install-old-chart: (_install-chart helm_name "./old-nginx")

# install the newer 4.2.1 ingress-nginx chart
install-new-chart: (_install-chart helm_name "./new-nginx")

# install the dummy web server
install-cors-test: (_install-chart "cors-test" "./cors-test")
