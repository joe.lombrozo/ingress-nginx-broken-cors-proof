# steps to reproduce the bug

1. `asdf install` (or make sure that every tool in .tool-versions has been installed)
2. `just init` to spin up the minikube cluster
3. run the test against the old controller
    a. `just install-old-chart` to install the old ingress-nginx controller
    b. `just install-cors-test` to install the dummy webserver
    c. `just test-get` to see CORS headers during a successful GET request
    d. `just test-post` to see CORS headers during a POST request rejected by nginx
4. run the test against the new controller by running steps 3a-3d, but replacing `install-old-chart` with `install-new-chart`

# expected behavior

The GET and POST should return identical headers:

```
Access-Control-Allow-Origin: http://localhost1
Access-Control-Allow-Credentials: true
Access-Control-Allow-Methods: GET, PUT, POST, DELETE, PATCH, OPTIONS
Access-Control-Allow-Headers: DNT,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization
```

# actual behavior (old)

The GET and POST only return the following headers:

```
Access-Control-Allow-Origin: *  # incorrect
Access-Control-Allow-Credentials: true  # correct
```

# actual behavior (new)

The GET works correctly.
The POST returns no CORS headers at all.
